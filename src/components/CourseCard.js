import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
//import { propTypes } from 'react-bootstrap/esm/Image';
import PropTypes from 'prop-types';

export default function CourseCard({courseProp}) {
    
    /*console.log("Content of props: ");
    console.log(props);
    console.log(typeof props);*/
        /*Very short version*/
    const {_id, name, description, price} = courseProp;

    // State hooks(useState) - a way to store information within a component and track this information

        //variable, function to change the value of a variable
    /*const [count, setCount] = useState(0);   //count = 0;
    const [seats, setSeats] = useState(30);

    function enroll(){
        if (seats > 0){
            setCount(count + 1);
            setSeats(seats - 1);
            console.log('Enrollees: ' + count);
            console.log('Seats: ' + seats);
        }
       else{
        alert('No more seats available.')
       }

        setCount(count + 1);
        setSeats(seats - 1);
        console.log('Enrollees: ' + count);
        console.log('Seats: ' + seats);
    }

     useEffect() always runs the task on the initial render and/or every render which is when the state changes in a component.
    /Initial render is when the component is run or displayed for the first time
    useEffect(() => {
        if(seats === 0){
            alert('No more seats available');
        }
    }, [seats]);
    
    */
    
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
            </Card.Body>
        </Card>
);
}

// PropTypes = Check if the CourseCard component is getting the correct property types
/*CourseCard.propTypes = {
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
}*/
