
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import { Container } from 'react-bootstrap';
// npm install react-router-dom
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom'

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error'
import CourseView from './pages/CourseView';

import { useState, useEffect, useContext } from 'react';
import { UserProvider } from './UserContext';

//Additional Example
import Settings from './pages/Settings';

function App() {
  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })

  // 1st step Create
  const unsetUser = () => {
    localStorage.clear();
  }


    // 2 Provide components
  return (


    <UserProvider value = {{user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element={<CourseView />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
